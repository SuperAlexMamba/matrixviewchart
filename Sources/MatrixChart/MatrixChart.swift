// The Swift Programming Language
// https://docs.swift.org/swift-book
//
//  MatrixView.swift
//  MatrixView
//
//  Created by Слава Орлов on 06.05.2024.
//

import Foundation
import UIKit

public final class Matrix: UIView {
	
	/// массив с данными
	public var dataArray: [Int] = []
	
	/// количество столбцов
	public var numberOfColumns: Int = 1
		
	/// количество строк
	public var numberOfRows: Int = 1
	
	/// расстояние между строками(вкл/выкл)
	public var isSpacing = false
	
	/// значение расстояния между строками
	public var spacing: CGFloat?
	
	/// ширина обводки
	public var cellLineWidth: CGFloat = 0.0
	
	/// обводка вкл/выкл
	public var setStroke = true
	
	/// закрашивание вкл/выкл
	public var setFill = false
	
	/// цвет обводки
	public var strokeColor: [UIColor] = [.black]
	
	/// цвет закрашивания
	public var fillColor: [UIColor] = [.white]
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupBackgroundColor()
	}
	
	required init?(coder: NSCoder) {
		super.init(coder: coder)
	}
	
	override public func draw(_ rect: CGRect) {
									
		let size = rect.width / CGFloat(numberOfColumns) - cellLineWidth / 10
		
		for row in 0..<numberOfRows {
			for column in 0..<numberOfColumns {
				let index = row * numberOfColumns + column
				drawCell(spacing: spacing ?? size,
						 size: size,
						 column: column,
						 row: numberOfRows - 1 - row,
						 strokeColor: strokeColor[index % strokeColor.count],
						 fillColor: fillColor[index % fillColor.count])
				
			}
		}
		self.setNeedsDisplay()
	}
	
	private func drawCell(spacing: CGFloat,
						  size: CGFloat,
						  column: Int,
						  row: Int,
						  strokeColor: UIColor?,
						  fillColor: UIColor?) {
		
		let x = (CGFloat(column) * size) + cellLineWidth
		var y = (CGFloat(row) * spacing) + cellLineWidth
		
		if isSpacing {
			if column % 2 != 1 {
				y += spacing / 2.0
			}
		}
		
		let origin = CGPoint(x: x + size/2.0, y: y + size/2.0)
		
		let path = UIBezierPath()
		for index in 0..<6 {
			let angle = CGFloat(index) * CGFloat.pi / 3
			let pointX = origin.x + size / 2.0 * cos(angle)
			let pointY = origin.y + size / 2.0 * sin(angle)
			if index == 0 {
				path.move(to: CGPoint(x: pointX, y: pointY))
			} else {
				path.addLine(to: CGPoint(x: pointX, y: pointY))
			}
		}
		
		path.close()
		
		setupCellColor(for: path,
					   strokeColor: strokeColor,
					   fillColor: fillColor)
		
	}
	
	private func setupCellColor(for path: UIBezierPath,
							 strokeColor: UIColor?,
							 fillColor: UIColor?) {
		if setStroke {
			strokeColor?.setStroke()
			path.lineWidth = cellLineWidth
			path.stroke()
		}
		if setFill {
			fillColor?.setFill()
			path.fill()
		}
	}
	
	private func setupBackgroundColor() {
		self.backgroundColor = .clear
	}
	
}
